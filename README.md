CurlDownload
============

Download file via PHP over curl

Usage:


```
#!php
require 'vendor/autoload.php';

$data = Vymakdevel\Download::synchroneDownloadOneFile('http://www.seznam.cz');
var_dump($data);


$urls = ['http://www.seznam.cz', 'http://www.google.com', 'http://www.zive.cz'];
$arrayDownload = Vymakdevel\Download::asynchroneDownloadDataByUrlArray($urls);
var_dump($arrayDownload);
```