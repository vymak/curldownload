<?php

namespace Vymakdevel;

use Nette\InvalidArgumentException;
use Nette\StaticClassException;
use Nette\Utils\Validators;

/**
 * @author Libor Vymětalík <l.vymetalik@vymakdevel.cz>
 * @version 1.2.3
 */
class Download
{

    public function __construct()
    {
        throw new StaticClassException("Cannot create object of static class");
    }

    /**
     * Synchroní stažení jednoho souboru
     * @param string $url
     * @param int $connectionTimeout
     * @param int $timeout
     * @return string|null
     */
    public static function synchroneDownloadOneFile($url, $connectionTimeout = 3000, $timeout = 10000)
    {
        self::checkUrl($url);
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_CONNECTTIMEOUT_MS => $connectionTimeout,
            CURLOPT_TIMEOUT_MS => $timeout,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE
        ));

        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * Asynchronní stažení dat pomocí zadané URL
     * @param string $url
     * @param int $page
     * @param int $limit
	 * @param int $step
     * @param int $connectionTimeout
     * @param int $timeout
     * @return array
     */
    public static function asynchroneDownloadDataByUrls($url, $page = 1, $limit = 100, $step = 1, $connectionTimeout = 3000, $timeout = 10000)
    {
        self::checkUrl($url);
        if (!Validators::isNumericInt($page) || !Validators::isNumericInt($limit)) {
            throw new InvalidArgumentException('Argument must be integer');
        }

        $urls = array();
        for ($i = $page; $i < $page + $limit; $i+= $step) {
            $adresa = $url . $i;
            $urls[$i] = $adresa;
        }
        return self::asynchroneDownloadData($urls, $connectionTimeout, $timeout);
    }

    /**
     * Asynchronní stažení dat pomocí pole adres
     * @param array $data
     * @param int $connectionTimeout
     * @param int $timeout
     * @return array
     */
    public static function asynchroneDownloadDataByUrlArray(array $data, $connectionTimeout = 3000, $timeout = 10000)
    {
        return self::asynchroneDownloadData($data, $connectionTimeout, $timeout);
    }

    /**
     * Asynchroní stažení dat pomocí pole ID a URL adresy
     * @param string $url
     * @param array $id
     * @param int $connectionTimeout
     * @param int $timeout
     * @return array
     */
    public static function asynchroneDownloadDataByUrlAndArray($url, array $id, $connectionTimeout = 3000, $timeout = 10000)
    {
        self::checkUrl($url);
        $urls = array();
        foreach ($id as $value) {
            $urls[$value] = $url . $value;
        }
        return self::asynchroneDownloadData($urls, $connectionTimeout, $timeout);
    }

    /**
     * Asynchroní stažení dat
     * @param array $data
     * @param int $connectionTimeout
     * @param int $timeout
     * @return array
     */
    private static function asynchroneDownloadData(array $data, $connectionTimeout, $timeout)
    {
        $curl_arr = array();
        $master = curl_multi_init();

        foreach ($data as $key => $value) {
            $curl_arr[$key] = curl_init($value);
            curl_setopt_array($curl_arr[$key], array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CONNECTTIMEOUT_MS => $connectionTimeout,
                CURLOPT_TIMEOUT_MS => $timeout,
                CURLOPT_SSL_VERIFYHOST => FALSE,
                CURLOPT_SSL_VERIFYPEER => FALSE
            ));
            curl_multi_add_handle($master, $curl_arr[$key]);
        }

        do {
            curl_multi_exec($master, $running);
        } while ($running > 0);


        $results = array();
        foreach ($data as $key => $value) {
            $tmp = curl_multi_getcontent($curl_arr[$key]);
            if (isset($tmp)) {
                $results[$key] = $tmp;
            }
        }
        return $results;
    }

    private static function checkUrl($url)
    {
        if (!Validators::isUrl($url)) {
            throw new InvalidArgumentException;
        }
    }

}
