<?php

namespace Test;

use Tester\Assert;
use Tester\TestCase;
use Vymakdevel\Download;

require_once '../vendor/autoload.php';

class DownloadTest extends TestCase
{

    public function testSynchroneDownload()
    {
        $data = Download::synchroneDownloadOneFile('http://www.seznam.cz');
        Assert::notEqual(NULL, $data);
        Assert::false(empty($data));

        $httpsData = Download::synchroneDownloadOneFile('https://www.unicreditbank.cz/web/exchange_rates_xml.php');
        Assert::truthy($httpsData);
    }

    public function testAsynchroneDownloadUrlArray()
    {
        $arr = array('http://www.seznam.cz', 'http://www.zive.cz', 'http://www.google.com');
        $data = Download::asynchroneDownloadDataByUrlArray($arr);
        Assert::false(empty($data));
        Assert::type('array', $data);
        Assert::same(3, count($data));

        foreach ($data as $value) {
            Assert::false(empty($value));
        }
    }

    public function testAsynchroneDownloadByUrlAndArray()
    {
        $arr = array('1800', '200', '300', '350', '230');
        $url = 'http://store.steampowered.com/app';

        $data = Download::asynchroneDownloadDataByUrlAndArray($url, $arr);
        Assert::false(empty($data));
        Assert::type('array', $data);
        Assert::same(count($arr), count($data));

        foreach ($data as $value) {
            Assert::false(empty($value));
        }
    }

    public function testAsynchroneDownloadDataByUrls()
    {
        $url = 'http://store.steampowered.com/app/';

        $data = Download::asynchroneDownloadDataByUrls($url, 1, 100);
        Assert::false(empty($data));
        Assert::type('array', $data);

        foreach ($data as $value) {
            Assert::false(empty($value));
        }
    }

}

$test = new DownloadTest();
$test->run();

